/*
	file automatically generated by make_test_files.pl
	Tue May 16 16:46:35 2023
*/

/*****************************************************************************
 *                                                                           *
 *          UNU.RAN -- Universal Non-Uniform Random number generator         *
 *                                                                           *
 *****************************************************************************/
    
/**
 ** Tests for STRINGPARSER
 **/
    
/*---------------------------------------------------------------------------*/
#include "testunuran.h"

#ifdef UNUR_URNG_DEFAULT_RNGSTREAM
#include <RngStream.h>
#endif
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* global variables                                                          */

static FILE *TESTLOG;               /* test log file                         */
static FILE *UNURANLOG;             /* unuran log file                       */

static int test_ok = TRUE;          /* all tests ok (boolean)                */
static int fullcheck = FALSE;       /* whether all checks are performed      */ 

static TIMER watch;                 /* stop watch                            */

/*---------------------------------------------------------------------------*/

void run_verify_generator( FILE *LOG, int line, UNUR_PAR *par );

int unur_stringparser_set_verify( UNUR_PAR *par, int verify);


/*---------------------------------------------------------------------------*/

void test_new (void);
void test_set (void);
void test_get (void);
void test_chg (void);
void test_init (void);
void test_reinit (void);
void test_sample (void);
void test_validate (void);
void test_special(void);

/*---------------------------------------------------------------------------*/



/* prototypes */
int unur_ssr_set_pedantic( struct unur_par *par, int pedantic );

#define COMPARE_SAMPLE_SIZE   (10000)
#define VIOLATE_SAMPLE_SIZE   (20)




/*---------------------------------------------------------------------------*/

#ifndef CHI2_FAILURES_TOLERATED
#  define CHI2_FAILURES_TOLERATED DEFAULT_CHI2_FAILURES_TOLERATED
#endif

/*---------------------------------------------------------------------------*/
/* [verbatim] */




/*---------------------------------------------------------------------------*/
/* [new] */

void test_new (void)
{
        int n_tests_failed;          /* number of failed tests */

	/* start test */
	printf("[new "); fflush(stdout);
	fprintf(TESTLOG,"\n[new]\n");

	/* reset counter */
	n_tests_failed = 0;

	/* set stop watch */
	stopwatch_lap(&watch);
  

	/* timing */
	stopwatch_print(TESTLOG,"\n<*>time = %.3f ms\n\n", stopwatch_lap(&watch));

	/* test finished */
	test_ok &= (n_tests_failed) ? 0 : 1;
	(n_tests_failed) ? printf(" ==> failed] ") : printf(" ==> ok] ");

} /* end of test_new() */

/*---------------------------------------------------------------------------*/
/* [init] */

void test_init (void)
{
        int n_tests_failed;          /* number of failed tests */

	/* start test */
	printf("[init "); fflush(stdout);
	fprintf(TESTLOG,"\n[init]\n");

	/* reset counter */
	n_tests_failed = 0;

	/* set stop watch */
	stopwatch_lap(&watch);
  
{ /* invalid NULL ptr */
UNUR_DISTR *distr = NULL;
UNUR_GEN   *gen = NULL;
   distr = NULL;
   gen = NULL; 


unur_reset_errno();
n_tests_failed += (check_expected_NULL(TESTLOG,45,(gen = unur_str2gen( NULL )))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,45,UNUR_ERR_NULL)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
n_tests_failed += (check_expected_NULL(TESTLOG,48,(distr = unur_str2distr( NULL )))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,48,UNUR_ERR_NULL)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
n_tests_failed += (check_expected_NULL(TESTLOG,51,(gen = unur_makegen_ssu(NULL,NULL,NULL)))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,51,UNUR_ERR_NULL)==UNUR_SUCCESS)?0:1;
unur_distr_free(distr);
unur_free(gen);
}

{ /* invalid distribution block */
UNUR_GEN   *gen = NULL;
   gen = NULL; 


unur_reset_errno();
n_tests_failed += (check_expected_NULL(TESTLOG,58,(gen = unur_str2gen( "xxxx" )))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,58,UNUR_ERR_STR_UNKNOWN)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
n_tests_failed += (check_expected_NULL(TESTLOG,62,(gen = unur_str2gen( "method = xxxx" )))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,62,UNUR_ERR_STR_SYNTAX)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
n_tests_failed += (check_expected_NULL(TESTLOG,66,(gen = unur_str2gen( "normal; xxxx=1" )))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,66,UNUR_ERR_STR_UNKNOWN)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
n_tests_failed += (check_expected_NULL(TESTLOG,70,(gen = unur_str2gen( "normal; pv=(1,2,3)" )))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,70,UNUR_ERR_STR_UNKNOWN)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
n_tests_failed += (check_expected_NULL(TESTLOG,74,(gen = unur_str2gen( "normal,(1,2)" )))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,74,UNUR_ERR_STR_UNKNOWN)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
n_tests_failed += (check_expected_NULL(TESTLOG,78,(gen = unur_str2gen( "beta" )))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,78,UNUR_ERR_STR_INVALID)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
n_tests_failed += (check_expected_NULL(TESTLOG,82,(gen = unur_str2gen( "beta()" )))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,82,UNUR_ERR_STR_INVALID)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
n_tests_failed += (check_expected_NULL(TESTLOG,86,(gen = unur_str2gen( "beta(1)" )))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,86,UNUR_ERR_STR_INVALID)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
gen = unur_str2gen( "beta(1,2,3)" );
n_tests_failed += (check_errorcode(TESTLOG,90,UNUR_ERR_DISTR_NPARAMS)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
gen = unur_str2gen( "beta(1,2,3,4,5)" );
n_tests_failed += (check_errorcode(TESTLOG,94,UNUR_ERR_DISTR_NPARAMS)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
n_tests_failed += (check_expected_NULL(TESTLOG,98,(gen = unur_str2gen( "gamma(-0.5)" )))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,98,UNUR_ERR_STR_INVALID)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
n_tests_failed += (check_expected_NULL(TESTLOG,102,(gen = unur_str2gen( "normal(); domain" )))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,102,UNUR_ERR_STR_INVALID)==UNUR_SUCCESS)?0:1;
unur_free(gen);
}

{ /* invalid other block */
UNUR_GEN   *gen = NULL;
   gen = NULL; 


unur_reset_errno();
n_tests_failed += (check_expected_NULL(TESTLOG,109,(gen = unur_str2gen( "normal() & xxxx = arou" )))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,109,UNUR_ERR_STR_UNKNOWN)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
n_tests_failed += (check_expected_NULL(TESTLOG,113,(gen = unur_str2gen( "normal() & distr=gamma(2)" )))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,113,UNUR_ERR_STR_UNKNOWN)==UNUR_SUCCESS)?0:1;
unur_free(gen);
}

{ /* invalid method block */
UNUR_GEN   *gen = NULL;
   gen = NULL; 


unur_reset_errno();
gen = unur_str2gen( "normal() & method = arou; c = 0" );
n_tests_failed += (check_errorcode(TESTLOG,120,UNUR_ERR_STR_UNKNOWN)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
n_tests_failed += (check_expected_NULL(TESTLOG,124,(gen = unur_str2gen( "normal() & method = xxx" )))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,124,UNUR_ERR_NULL)==UNUR_SUCCESS)?0:1;
unur_free(gen);
}

{ /* invalid urng block */
UNUR_GEN   *gen = NULL;
    gen = NULL; 


unur_reset_errno();
gen = unur_str2gen( "normal & urng = xxx" );
n_tests_failed += (check_errorcode(TESTLOG,131,UNUR_ERR_STR)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
#ifdef UNURAN_SUPPORTS_PRNG
gen = unur_str2gen( "normal & urng = mt19937(123); xxx = 1" );
n_tests_failed += (check_errorcode(TESTLOG,136,UNUR_ERR_STR_UNKNOWN)==UNUR_SUCCESS)?0:1;
#endif
unur_free(gen);
}

{ /* invalid data */
UNUR_GEN   *gen = NULL;
   gen = NULL; 


unur_reset_errno();
n_tests_failed += (check_expected_NULL(TESTLOG,145,(gen = unur_str2gen( "gamma(0.5) & method = tdr" )))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,145,UNUR_ERR_GEN_DATA)==UNUR_SUCCESS)?0:1;
unur_free(gen);
}

{ /* invalid data */
UNUR_DISTR *distr = NULL;
UNUR_PAR   *par = NULL;
UNUR_GEN   *gen = NULL;
   struct unur_slist *mlist = NULL;
   distr = NULL;
   par = NULL;
   gen = NULL; 

distr = unur_str2distr("normal()");
par = _unur_str2par(distr, "tdr", &mlist);
gen = unur_init(par); 
_unur_slist_free(mlist);
unur_distr_free(distr);
unur_free(gen);
}

{ /* invalid distribution block */
UNUR_DISTR *distr = NULL;
   distr = NULL; 


unur_reset_errno();
n_tests_failed += (check_expected_NULL(TESTLOG,168,(distr = unur_str2distr("cont; pdf='exp'")))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,168,UNUR_ERR_STR_INVALID)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
n_tests_failed += (check_expected_NULL(TESTLOG,173,(distr = unur_str2distr("cont; pdf='abc(x)'")))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,173,UNUR_ERR_STR_INVALID)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
n_tests_failed += (check_expected_NULL(TESTLOG,178,(distr = unur_str2distr("cont; pdf='exp(x*y)'")))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,178,UNUR_ERR_STR_INVALID)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
n_tests_failed += (check_expected_NULL(TESTLOG,182,(distr = unur_str2distr("cont; pdf='exp(x'")))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,182,UNUR_ERR_STR_INVALID)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
n_tests_failed += (check_expected_NULL(TESTLOG,185,(distr = unur_str2distr("cont; pdf='exp((x)'")))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,185,UNUR_ERR_STR_INVALID)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
n_tests_failed += (check_expected_NULL(TESTLOG,188,(distr = unur_str2distr("cont; pdf='exp(x))'")))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,188,UNUR_ERR_STR_INVALID)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
n_tests_failed += (check_expected_NULL(TESTLOG,192,(distr = unur_str2distr("cont; pdf='x^'")))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,192,UNUR_ERR_STR_INVALID)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
n_tests_failed += (check_expected_NULL(TESTLOG,196,(distr = unur_str2distr("cont; pdf='exp(x)x'")))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,196,UNUR_ERR_STR_INVALID)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
n_tests_failed += (check_expected_NULL(TESTLOG,199,(distr = unur_str2distr("cont; pdf='2x'")))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,199,UNUR_ERR_STR_INVALID)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
n_tests_failed += (check_expected_NULL(TESTLOG,202,(distr = unur_str2distr("cont; pdf='x***2'")))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,202,UNUR_ERR_STR_INVALID)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
n_tests_failed += (check_expected_NULL(TESTLOG,205,(distr = unur_str2distr("cont; pdf='x*exp(x^(x*y))'")))==UNUR_SUCCESS)?0:1;
n_tests_failed += (check_errorcode(TESTLOG,205,UNUR_ERR_STR_INVALID)==UNUR_SUCCESS)?0:1;
unur_distr_free(distr);
}


	/* timing */
	stopwatch_print(TESTLOG,"\n<*>time = %.3f ms\n\n", stopwatch_lap(&watch));

	/* test finished */
	test_ok &= (n_tests_failed) ? 0 : 1;
	(n_tests_failed) ? printf(" ==> failed] ") : printf(" ==> ok] ");

} /* end of test_init() */

/*---------------------------------------------------------------------------*/
/* [sample] */

void test_sample (void)
{
        int n_tests_failed;          /* number of failed tests */

	/* start test */
	printf("[sample "); fflush(stdout);
	fprintf(TESTLOG,"\n[sample]\n");

	/* reset counter */
	n_tests_failed = 0;

	/* set stop watch */
	stopwatch_lap(&watch);
  
{ /* compare function parser - cont */
UNUR_DISTR *distr = NULL;
UNUR_PAR   *par = NULL;
UNUR_GEN   *gen = NULL;
   double fpm[10];
   distr = NULL;
   par = NULL;
   gen = NULL; 


unur_reset_errno();
distr = unur_distr_normal(NULL,0); 
par = unur_srou_new(distr);
gen = unur_init(par);
n_tests_failed += (compare_sequence_gen_start(TESTLOG,231,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_str2gen( "cont; pdf = \"(1/sqrt(2*pi)*exp(-x^2/2))\"; mode = 0; pdfarea = 1 & \
	             method = srou" );
n_tests_failed += (compare_sequence_gen(TESTLOG,237,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_str2gen( "cont; logpdf = \"-x^2/2-0.5*log(2*pi)\"; mode = 0; pdfarea = 1 & \
	             method = srou" );
n_tests_failed += (compare_sequence_gen(TESTLOG,243,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_makegen_ssu( "cont; logpdf = \"-x^2/2-0.5*log(2*pi)\"; mode = 0; pdfarea = 1", \
	             "method = srou", NULL );
n_tests_failed += (compare_sequence_gen(TESTLOG,249,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_makegen_ssu( "cont; logpdf = \"-x^2/2-0.5*log(2*pi)\"; mode = 0; pdfarea = 1", \
	             "srou", NULL );
n_tests_failed += (compare_sequence_gen(TESTLOG,255,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
distr = unur_str2distr("cont; logpdf = \"-x^2/2-0.5*log(2*pi)\"; mode = 0; pdfarea = 1");
gen = unur_makegen_dsu( distr, "srou", NULL );
n_tests_failed += (compare_sequence_gen(TESTLOG,262,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
distr = unur_distr_normal(NULL,0); 
par = unur_ssr_new(distr);
gen = unur_init(par);
n_tests_failed += (compare_sequence_gen_start(TESTLOG,271,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_str2gen( "cont; pdf = \"(1/sqrt(2*pi)*exp(-x^2/2))\"; mode = 0; pdfarea = 1 & \
	             method = ssr" );
n_tests_failed += (compare_sequence_gen(TESTLOG,278,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
distr = unur_distr_normal(NULL,0); 
par = unur_utdr_new(distr);
gen = unur_init(par);
n_tests_failed += (compare_sequence_gen_start(TESTLOG,287,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_str2gen( "cont; pdf = \"(1/sqrt(2*pi)*exp(-x^2/2))\"; mode = 0; pdfarea = 1 & \
	             method = utdr" );
n_tests_failed += (compare_sequence_gen(TESTLOG,293,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_str2gen( "cont; logpdf = \"-x^2/2-0.5*log(2*pi)\"; mode = 0; pdfarea = 1 & \
	             method = utdr" );
n_tests_failed += (compare_sequence_gen(TESTLOG,299,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
distr = unur_distr_normal(NULL,0); 
par = unur_tdr_new(distr);
unur_tdr_set_cpoints(par,8,NULL);
unur_tdr_set_max_sqhratio(par,0.);
gen = unur_init(par);
n_tests_failed += (compare_sequence_gen_start(TESTLOG,310,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_str2gen( "cont; pdf = \"(1/sqrt(2*pi)*exp(-x^2/2))\"; mode = 0; pdfarea = 1 & \
	             method = tdr; cpoints = 8; max_sqhratio = 0." );
n_tests_failed += (compare_sequence_gen(TESTLOG,316,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_str2gen( "cont; logpdf = \"-x^2/2-0.5*log(2*pi)\"; mode = 0; pdfarea = 1 & \
	             method = tdr; cpoints = 8; max_sqhratio = 0." );
n_tests_failed += (compare_sequence_gen(TESTLOG,322,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_makegen_ssu( "cont; logpdf = \"-x^2/2-0.5*log(2*pi)\"; mode = 0; pdfarea = 1", \
	             "method = tdr; cpoints = 8; max_sqhratio = 0.", NULL );
n_tests_failed += (compare_sequence_gen(TESTLOG,328,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_makegen_ssu( "cont; logpdf = \"-x^2/2-0.5*log(2*pi)\"; mode = 0; pdfarea = 1", \
	             "tdr; cpoints = 8; max_sqhratio = 0.", NULL );
n_tests_failed += (compare_sequence_gen(TESTLOG,334,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
distr = unur_distr_normal(NULL,0); 
par = unur_arou_new(distr);
unur_arou_set_cpoints(par,8,NULL);
unur_arou_set_max_sqhratio(par,0.);
gen = unur_init(par);
n_tests_failed += (compare_sequence_gen_start(TESTLOG,345,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_str2gen( "cont; pdf = \"(1/sqrt(2*pi)*exp(-x^2/2))\"; mode = 0; pdfarea = 1 & \
	             method = arou; cpoints = 8; max_sqhratio = 0." );
n_tests_failed += (compare_sequence_gen(TESTLOG,351,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
distr = unur_distr_cont_new();
unur_distr_cont_set_cdfstr(distr,"1-exp(-x)");
par = unur_ninv_new(distr);
unur_ninv_set_usenewton(par);
gen = unur_init(par);
n_tests_failed += (compare_sequence_gen_start(TESTLOG,362,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_str2gen( "cont; cdf = \"1-exp(-x)\"; domain = (0,inf)& \
	             method = ninv; usenewton" );
n_tests_failed += (compare_sequence_gen(TESTLOG,368,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
distr = unur_distr_cont_new();
unur_distr_cont_set_hrstr(distr,"1/(1+x)");
par = unur_hrb_new(distr);
gen = unur_init(par);
n_tests_failed += (compare_sequence_gen_start(TESTLOG,378,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_str2gen( "cont; hr = \"1/(1+x)\"; domain = (0,inf)& \
	             method = hrb" );
n_tests_failed += (compare_sequence_gen(TESTLOG,384,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
distr = unur_distr_normal(NULL,0);
par = unur_auto_new(distr);
gen = unur_init(par);
n_tests_failed += (compare_sequence_gen_start(TESTLOG,393,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_str2gen( "normal() & method=auto" );
n_tests_failed += (compare_sequence_gen(TESTLOG,398,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_makegen_ssu( "normal()", "method=auto", NULL );
n_tests_failed += (compare_sequence_gen(TESTLOG,403,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_makegen_ssu( "normal()", "auto", NULL );
n_tests_failed += (compare_sequence_gen(TESTLOG,408,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_makegen_ssu( "normal()", "", NULL );
n_tests_failed += (compare_sequence_gen(TESTLOG,413,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_makegen_ssu( "normal()", NULL, NULL );
n_tests_failed += (compare_sequence_gen(TESTLOG,418,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_makegen_ssu( "normal", NULL, NULL );
n_tests_failed += (compare_sequence_gen(TESTLOG,423,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
fpm[0] = 1.;
fpm[1] = 2.;
distr = unur_distr_normal(fpm,2);
par = unur_auto_new(distr);
gen = unur_init(par);
n_tests_failed += (compare_sequence_gen_start(TESTLOG,434,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_str2gen( "normal(1.,2.) & method=auto" );
n_tests_failed += (compare_sequence_gen(TESTLOG,439,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_makegen_ssu( "normal(1,2)", "method=auto", NULL );
n_tests_failed += (compare_sequence_gen(TESTLOG,444,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_makegen_ssu( "normal(1,2.)", "auto", NULL );
n_tests_failed += (compare_sequence_gen(TESTLOG,449,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_makegen_ssu( "normal(1.,2)", "", NULL );
n_tests_failed += (compare_sequence_gen(TESTLOG,454,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_makegen_ssu( "normal(1,2)", NULL, NULL );
n_tests_failed += (compare_sequence_gen(TESTLOG,459,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
fpm[0] = 1.;
fpm[1] = 2.;
distr = unur_distr_normal(fpm,2);
gen = unur_makegen_dsu( distr, "method=auto", NULL );
n_tests_failed += (compare_sequence_gen(TESTLOG,467,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
fpm[0] = 1.;
fpm[1] = 2.;
distr = unur_distr_normal(fpm,2);
gen = unur_makegen_dsu( distr, "auto", NULL );
n_tests_failed += (compare_sequence_gen(TESTLOG,475,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
fpm[0] = 1.;
fpm[1] = 2.;
distr = unur_distr_normal(fpm,2);
gen = unur_makegen_dsu( distr, "", NULL );
n_tests_failed += (compare_sequence_gen(TESTLOG,483,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
fpm[0] = 1.;
fpm[1] = 2.;
distr = unur_distr_normal(fpm,2);
gen = unur_makegen_dsu( distr, NULL, NULL );
n_tests_failed += (compare_sequence_gen(TESTLOG,491,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;
unur_distr_free(distr);
unur_free(gen);
}

{ /* compare function parser - discr */
UNUR_DISTR *distr = NULL;
UNUR_PAR   *par = NULL;
UNUR_GEN   *gen = NULL;
   double fpar[] = {0.4};
   distr = NULL;
   par = NULL;
   gen = NULL; 


unur_reset_errno();
distr = unur_distr_geometric(fpar,1); 
par = unur_dsrou_new(distr);
gen = unur_init(par);
n_tests_failed += (compare_sequence_gen_start(TESTLOG,510,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_str2gen( "discr; pmf = \"0.4 * (1-0.4)^k\"; domain = (0,inf); mode = 0; pmfsum = 1 & \
	             method = dsrou" );
n_tests_failed += (compare_sequence_gen(TESTLOG,516,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
distr = unur_distr_geometric(fpar,1); 
par = unur_dau_new(distr);
gen = unur_init(par);
n_tests_failed += (compare_sequence_gen_start(TESTLOG,524,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_str2gen( "discr; pmf = \"0.4 * (1-0.4)^k\"; domain = (0,inf); mode = 0; pmfsum = 1 & \
	             method = dau" );
n_tests_failed += (compare_sequence_gen(TESTLOG,530,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
distr = unur_distr_geometric(fpar,1); 
par = unur_dgt_new(distr);
gen = unur_init(par);
n_tests_failed += (compare_sequence_gen_start(TESTLOG,538,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_str2gen( "discr; pmf = \"0.4 * (1-0.4)^k\"; domain = (0,inf); mode = 0; pmfsum = 1 & \
	             method = dgt" );
n_tests_failed += (compare_sequence_gen(TESTLOG,544,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
distr = unur_distr_geometric(fpar,1); 
par = unur_dari_new(distr);
gen = unur_init(par);
n_tests_failed += (compare_sequence_gen_start(TESTLOG,552,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;

unur_reset_errno();
unur_free(gen); gen = NULL;
unur_distr_free(distr); distr = NULL;
gen = unur_str2gen( "discr; pmf = \"0.4 * (1-0.4)^k\"; domain = (0,inf); mode = 0; pmfsum = 1 & \
	             method = dari" );
n_tests_failed += (compare_sequence_gen(TESTLOG,559,gen,COMPARE_SAMPLE_SIZE)==UNUR_SUCCESS)?0:1;
unur_distr_free(distr);
unur_free(gen);
}


	/* timing */
	stopwatch_print(TESTLOG,"\n<*>time = %.3f ms\n\n", stopwatch_lap(&watch));

	/* test finished */
	test_ok &= (n_tests_failed) ? 0 : 1;
	(n_tests_failed) ? printf(" ==> failed] ") : printf(" ==> ok] ");

} /* end of test_sample() */


/*---------------------------------------------------------------------------*/
/* run generator in verifying mode */

void run_verify_generator( FILE *LOG, int line, UNUR_PAR *par )
{
	UNUR_GEN *gen;
	int i;

	/* switch to verifying mode */
	unur_stringparser_set_verify(par,1);

	/* initialize generator */
	gen = unur_init( par ); abort_if_NULL(LOG, line, gen);

	/* run generator */
	for (i=0; i<VIOLATE_SAMPLE_SIZE; i++)
		unur_sample_cont(gen);

	/* destroy generator */
	unur_free(gen); 

} /* end of run_verify_generator() */

int unur_stringparser_set_verify(UNUR_PAR *par ATTRIBUTE__UNUSED, int verify ATTRIBUTE__UNUSED) {return 0;}

/*---------------------------------------------------------------------------*/

int main(void)
{ 
        unsigned long seed;
	char *str_seed, *str_tail;

	/* start stop watch */
	stopwatch_init();
	stopwatch_start(&watch);

        /* open log file for unuran and set output stream for unuran messages */
        UNURANLOG = fopen( "t_stringparser_unuran.log","w" );
        abort_if_NULL( stderr,-1, UNURANLOG );
        unur_set_stream( UNURANLOG );

        /* open log file for testing */
	TESTLOG = fopen( "t_stringparser_test.log","w" );
	abort_if_NULL( stderr,-1, TESTLOG );

        /* seed for uniform generators */

	/* seed set by environment */
	str_seed = getenv("SEED");

	if (str_seed != NULL) {
	    seed = strtol(str_seed, &str_tail, 10);
	    if (seed == 0u) 
		seed = 697012;
	}
	else {
#ifdef SEED
	    seed = SEED;
#else
	    seed = 697012;
#endif
	}

        /* seed build-in uniform generators */
        unur_urng_MRG31k3p_seed(NULL,seed);
        unur_urng_fish_seed(NULL,seed);
	unur_urng_mstd_seed(NULL,seed);

	/* seed uniform random number generator */
#ifdef UNUR_URNG_UNURAN
#  ifdef UNUR_URNG_DEFAULT_RNGSTREAM
	{
	        unsigned long sa[6];
	        int i;
	        for (i=0; i<6; i++) sa[i] = seed;
                RngStream_SetPackageSeed(sa);
        }
#  else
	if (unur_urng_seed(NULL,seed) != UNUR_SUCCESS) {
	        fprintf(stderr,"WARNING: Seed could not be set at random\n");
                seed = ~0u;
	}
#  endif  /* UNUR_URNG_DEFAULT_RNGSTREAM */
#endif  /* UNUR_URNG_UNURAN */
 
	/* set default debugging flag */
	unur_set_default_debug(UNUR_DEBUG_ALL);

        /* detect required check mode */
        fullcheck = (getenv("UNURANFULLCHECK")==NULL) ? FALSE : TRUE;

	/* write header into log file */
        print_test_log_header( TESTLOG, seed, fullcheck );

	/* set timer for sending SIGALRM signal */
	set_alarm(TESTLOG);

	/* start test */
	printf("stringparser: ");

	/* run tests */
test_new();
test_init();
test_sample();


	/* test finished */
	printf("\n");  fflush(stdout);

	/* close log files */
	fprintf(TESTLOG,"\n====================================================\n\n");
	if (test_ok)
		fprintf(TESTLOG,"All tests PASSED.\n");
	else
		fprintf(TESTLOG,"Test(s) FAILED.\n");

	/* timing */
	stopwatch_print(TESTLOG,"\n<*>total time = %.0f ms\n\n", stopwatch_stop(&watch));

	fclose(UNURANLOG);
	fclose(TESTLOG);

	/* free memory */
	compare_free_memory();
	unur_urng_free(unur_get_default_urng());
	unur_urng_free(unur_get_default_urng_aux());

	/* exit */
	exit( (test_ok) ? EXIT_SUCCESS : EXIT_FAILURE );

} /* end of main */

